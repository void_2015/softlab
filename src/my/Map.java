package my;

import java.io.InputStreamReader;
import java.util.ArrayList;

public class Map {

    //Lista, amiben tároljuk a játékban szereplő karaktereket
	private ArrayList<Character> characters;
    //A pályaelemeket tartalmazó lista.
    private ArrayList<Road> roads;

    public Map(){
        Main.logger.lock();
            characters = new ArrayList<Character>();
            roads = new ArrayList<Road>();
            Character ch = new Character();
            characters.add(ch);
            Road road = new Road();
            roads.add(road);
        Main.logger.unlock();
    }

    //Egy fájlból beolvassa a pályát, és visszaadja a kezdőmezőt
	public void loadMap(String file) {
        Main.logger.inc();
        Main.logger.printLog("loadMap("+ file +")");
	}

    //Ellenőrzi a karakterek, akadályok pozícióját. Pálya elhagyás, ütközés esetén elvégzi a megfelelő műveleteket.
    public void check() {
        //Loggoláshoz
        Main.logger.inc();
        Main.logger.printLog("check()");
        Main.logger.nextLevel();
        Character ch=characters.get(0);
            Main.logger.inc();
            Main.logger.printLog("get(0)"); //a list get függvényének Loggolása
            System.out.println("<--ch:Character");
        Road r = roads.get(0);
            Main.logger.inc();
            Main.logger.printLog("get(0)");
            System.out.println("<--r:Road");
        Rect chRect = ch.getRect();
        System.out.println("<--chRect:Rect");
        Rect rr = r.getRect();
        System.out.println("<--rr:Rect");
        rr.intersect(chRect);

        System.out.println(">>Kiment a palyarol? [i/n]");

        String a = Main.br.next();

        if(a.equalsIgnoreCase("n"))
        {
            ch.setCurrentRoad(r);
            detectCollision(r,ch);
        }
        else {
            ch.destroy();
        }

	}

    //Elkéri a Road példánytól az akadályait és ellenőrzi történt-e ütközés. Ha igen elvégzi a megfelelő műveletet.
	public void detectCollision(Road r, Character ch) {
        //Loggoláshoz
        Main.logger.inc();
        Main.logger.printLog("detectCollision()");
        Main.logger.nextLevel();

        Obstacle obst=r.getObstacle(0);
            System.out.println("<--obst:Obstacle");
        Rect obr = obst.getRect();
            System.out.println("<--obr:Rect");
        Rect chRect= ch.getRect();
            System.out.println("<--chRect:Rect");
        chRect.intersect(obr);

        System.out.println(">>Rament az akadalyra? [i/n]");

        String a = Main.br.next();

        if(a.equalsIgnoreCase("i"))
        {
            System.out.println(">>Milyen akadalyra lepett? r:ragacs , o:olaj , l:lyuk , s:szikla");
            String b = Main.br.next();
            if(b.equalsIgnoreCase("r")){
                Gum g = new Gum();
                g.execute(ch);
            }else if(b.equalsIgnoreCase("o")){
                Oil o = new Oil();
                o.execute(ch);
            }else if(b.equalsIgnoreCase("l")){
                Hole h = new Hole();
                h.execute(ch);
            }else if(b.equalsIgnoreCase("s")){
                Rock ro = new Rock();
                ro.execute(ch);
            }

        }
        else {
            return;
        }


    }

}
