package my;

import java.util.ArrayList;

public class Character {
    //Maga a sebességvektor. Meghatározza a robot sebességének nagyságát és irányát
    private Velocity velocity;
    //Lista, melyben tároljuk az Obstacle példányokat
    private ArrayList <CharacterObstacle> obstacles;
    //A távolság, amit megtett a játék során
	private int distance;
    //Ha igaz, nem tudja változtatni a sebességét (erre olajfoltra futás esetén van szükség)
    private boolean constantvelocity;
    //Tartalmazza a Rect osztályt, ez tárolja a pozícióját.
    private Rect rect;
    //Referencia arra az útdarabra, amin a karakter épp tartózkodik
    private Road currentRoad;

    //Az osztály konstruktora
    Character(){
        //Loggoláshoz
        Main.logger.inc();
        Main.logger.printLog("Character()");
        Main.logger.nextLevel();

        //Attribútumok inicializálása
        velocity = new Velocity();
        obstacles = new ArrayList<CharacterObstacle>();
        obstacles.add(ObstacleFactory.getCharacterObstacle());
        distance = 0;
        constantvelocity = false;
        rect = new Rect();
        currentRoad = new Road();
	}

    //Ennek a függvénynek a segítségével lehet lerakni akadályt a pályára.
	public void putObstacle() {
        //Loggoláshoz
        Main.logger.inc();
        Main.logger.printLog("putObstacle()");
        Main.logger.nextLevel();

        //Ha nincs mit lerakni, nem történik semmi
        if(obstacles.isEmpty()){
            return;
        }

        //A tár első eleme megkapja a karakter pozícióját, majd az obstaclet átadja az útnak
        Obstacle o = obstacles.remove(0);
		double x = rect.getX();
		double y = rect.getY();
		Rect r = o.getRect();
		r.setPosition(x,y);
		currentRoad.addObstacle(o);
	}

    //A character önmegsemmisítő metódusa
    public void destroy() {
        //Loggoláshoz
        Main.logger.inc();
        Main.logger.printLog("destroy()");
    }

    //Periodikusan hívva beállítja a character új pozícióját a sebesség vektor komponenseinek értéke alapján
    public void changePosition() {
        //Loggoláshoz
        Main.logger.inc();
        Main.logger.printLog("changePosition()");
        Main.logger.nextLevel();

        double x= velocity.getX();
        System.out.println("<--x:double");
        double y= velocity.getY();
        System.out.println("<--y:double");
        rect.setPosition(x,y);
    }

    //A paraméterül kapott irány alapján módosítja a sebességvektor komponenseit
    public void move(Direction direction) {
        Main.logger.inc();
        Main.logger.printLog("move(" +direction+ ")");
        Main.logger.nextLevel();
        System.out.println(">>Melyik iranyba mozogjon? [w-fel , s-le , a-balra , d-jobbra]");


        //Megkérdezzük a felhasználót merre szeretne menni
        String a = Main.br.next();
        if(a.equalsIgnoreCase("w")) {
            velocity.addX(1);
        }else if(a.equalsIgnoreCase("s")){
            velocity.addX(-1);
        }else if(a.equalsIgnoreCase("a")){
            velocity.addY(1);
        }else if(a.equalsIgnoreCase("d")){
            velocity.addY(-1);
        }

    }

    //A distance attribútum getter metódusa
	public int getDistance() {
        //Loggoláshoz
		Main.logger.inc();
        Main.logger.printLog("getDistance()");

		return distance;
	}

    //A constantvelocity setter metódusa
	public void setConstantVelocity(boolean b) {
		//Loggoláshoz
        Main.logger.inc();
		Main.logger.printLog("setConstantVelocity( " + b + " )");

        constantvelocity = b;
	}

    //A rect getter metódusa
	public Rect getRect() {
        //Loggoláshoz
        Main.logger.inc();
        Main.logger.printLog("getRect()");

		return rect;
	}

    //A velocity getter metódusa
	public Velocity getVelocity() {
        //Loggoláshoz
        Main.logger.inc();
        Main.logger.printLog("getVelocity()");

		return velocity;
	}

    //A currentRoad setter metódusa
	public void setCurrentRoad(Road r) {
        //Loggoláshoz
        Main.logger.inc();
        Main.logger.printLog("setCurrentRoad()");

	}



}
