package my;

public class ObstacleFactory {
	public static Obstacle getObstacle() {
		Main.logger.inc();
		Main.logger.printLog("getObstacle()");
		Main.logger.nextLevel();
		return new Gum();
	}

	public static CharacterObstacle getCharacterObstacle() {
        Main.logger.inc();
        Main.logger.printLog("getCharacterObstacle()");
        Main.logger.nextLevel();
        return new Gum();
	}

}
