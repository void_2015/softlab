package my;

import java.io.InputStreamReader;
import java.util.Scanner;

public class Main {
    static Logger logger = new Logger();
    static Scanner br = new Scanner(new InputStreamReader(System.in));

    public static void main(String[] args) {
        try {
            int which = 0;
            while (which != 7) {
                System.out.println("===================================================");
                System.out.println("MarsOne.Szkeleton #void");
                System.out.println("===================================================");
                System.out.println("***FoMenu***");
                System.out.println(">>1. ChangeVelocity");
                System.out.println(">>2. NewGame");
                System.out.println(">>3. LeaveField");
                System.out.println(">>4. PutObstacle");
                System.out.println(">>5. ChangePosition");
                System.out.println(">>6. StepOnObstacle");
                System.out.println(">>7. Kilepes");
                System.out.println("***");
                System.out.println("Valasztott menupont:");


                if (br.hasNextInt())
                    which = br.nextInt();
                else
                    br.next();


                logger.lock();
                Map map = new Map();
                Rock rock = new Rock();
                Road road = new Road();
                Character ch = new Character();
                logger.unlock();
                logger.reset();


                switch (which) {
                    case 1:
                        System.out.println("***Test1 - ChangeVelocity***");
                        System.out.println("A Velocity osztaly megfelelo iranyhoz tartozo fuggvenyet hasznaljuk a sebesseg megvaltoztatasara.\n " +
                                "Amely megfelelo iranyba modositja a szukseges koordinatakat. Ha nem kap sebessegvaltoztatasra vonatkozo informaciokat " +
                                "akkor nem fog lassulni, sebessege megmarad.");
                        logger.reset();

                        ch.move(Direction.DOWN);

                        break;
                    case 2:
                        System.out.println("***Test2 - NewGame***");
                        System.out.println("Uj jatek inditasakor a program betolti a palyat, betolti a szukseges\n " +
                                "objektumokat, gondoskodik az akadalyok elhelyezeserol. Az alapok elkesziteset,\n " +
                                "osszehangolasat itt ellenorizhetjuk.");
                        logger.reset();
                        map.loadMap("");
                        break;
                    case 3:
                        System.out.println("***Test3 - LeaveField***");
                        System.out.println("A palya esetleges elhagyasat a Map osztaly chech fuggvenyevel ellenorizzuk,\n " +
                                "ez vegigmegy az osszes jatekban levo karakteren es ellenorzi hogy rajta talalhato e valamely palyaelemen,\n " +
                                "ha rajta talalhato akkor frissitjuk az utdarabot, ha nem akkor meghivjuk a destroy fuggvenyet.");
                        logger.reset();
                        map.check();
                        break;
                    case 4:
                        System.out.println("***Test4 - PutObstacle***");
                        System.out.println("A karakter az altala tarolt akadalyok kozul kiveszi a soron kovetkezot majd beallitja a poziciojat\n " +
                                "az aktualis helyere, majd hozzaadja az aktualis utdarab akadalyaihoz.");
                        logger.reset();
                        ch.putObstacle();
                        break;
                    case 5:
                        System.out.println("***Test5 - ChangePosition***");
                        System.out.println("A karakter lekerdezi sajat sebessegvektoranak x,y ertekeit majd ezen parameterekkel meghivja\n a sajat Rect osztalyanak " +
                                "setPosition fuggvenyet.");
                        logger.reset();
                        ch.changePosition();
                        break;
                    case 6:
                        System.out.println("***Test6 - StepOnObstacle***");
                        System.out.println("Az akadalyra lepes ellenorzesehez a Map osztaly detectCollisioon fuggvenyere van szukseg,\n " +
                                "amely ellenorzi hogy azonos pozicion van e az akadaly es a karakter. Ha igen akkor meghivja\n az adott akadaly execute fuggvenyet " +
                                "melynek atadja a karaktert.");
                        logger.reset();
                        map.detectCollision(road, ch);
                        break;
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}