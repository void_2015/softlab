package my;

public class Velocity {

	private double vx=0;
    private double vy=0;

	Velocity(){
		Main.logger.inc();
        Main.logger.printLog("Velocity()");
	}
	public void addX(double x) {
        Main.logger.inc();
        Main.logger.printLog("addX( "+x+" )");
	}

	public void addY(double y) {
        Main.logger.inc();
        Main.logger.printLog("addY( "+y+" )");
	}

	public double getX() {
        Main.logger.inc();
        Main.logger.printLog("getX()");
		return 0;
	}

	public double getY() {
        Main.logger.inc();
        Main.logger.printLog("getY()");
		return 0;
	}

	public void setVelocity(double x, double y) {
        Main.logger.inc();
        Main.logger.printLog("setVelocity( " + x + " " + y + " )");
	}

}
