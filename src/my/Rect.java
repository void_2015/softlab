package my;

public class Rect {

	private double x;

	private double y;

	private double v;

	private double h;
	Rect(){
		Main.logger.inc();
		Main.logger.printLog("Rect()");
	}
	public void setPosition(double x, double y) {
		this.x=x;
		this.y=y;
		Main.logger.inc();
		Main.logger.printLog("setPosition( " + x + ", " + y + " )");
	}

	public boolean intersect(Rect other) {
		Main.logger.inc();
        Main.logger.printLog("intersect(" +other+ ")");
        return false;
	}

	public double getX() {
		Main.logger.inc();
		Main.logger.printLog("getX()");
		return x;
	}

	public double getY() {
		Main.logger.inc();
		Main.logger.printLog("getY()");
		return y;
	}

}
