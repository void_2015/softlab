package my;

public class Oil extends CharacterObstacle {

	public void execute(my.Character ch) {
		Main.logger.inc();
		Main.logger.printLog("execute( "+ch+" )");
		Main.logger.nextLevel();
		ch.setConstantVelocity(true);
		
	}

}
