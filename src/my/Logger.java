package my;

import java.util.ArrayList;

public class Logger {

    Logger(){
        levels = new ArrayList<Integer>();
        index = 0;
        lockCount=0;
        locked=false;

    }


    void inc(){
        if(locked==false) {
            if (levels.isEmpty())
                levels.add(0);
            levels.set(index, levels.get(index) + 1);
        }
    }

    void nextLevel(){
        if(locked==false) {
            index++;
            levels.add(0);
        }
    }

    void prevLevel(){
        if(locked==false) {
            index--;
        }
    }
    void printLog(String function){
        if(locked==false) {
            for (int i = 0; i <= index - 1; i++)
                System.out.print("\t");
            for (int i = 0; i <= index; i++)
                System.out.printf("%d.", levels.get(i));
            System.out.println(function);
        }
    }

    void reset(){
        if(locked==false) {
            index = 0;
            levels = new ArrayList<Integer>();
        }
    }

    void lock(){
        lockCount++;
        locked=true;

    }

    void unlock(){
        lockCount--;
        if(lockCount==0)
            locked=false;

    }

    private StringBuffer function;
    int index;
    private ArrayList<Integer> levels ;
    private boolean locked;
    private int lockCount;
}
