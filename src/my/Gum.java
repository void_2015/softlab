package my;

public class Gum extends CharacterObstacle {

	public void execute(Character ch) {
			Main.logger.inc();
			Main.logger.printLog("execute( "+ch+" )");
			Main.logger.nextLevel();
			Velocity v = ch.getVelocity();
            System.out.println("<--v:Velocity");
			double x = v.getX();
            System.out.println("<--x:double");
			double y = v.getY();
            System.out.println("<--y:double");
			v.setVelocity(x,y);
	}

}
