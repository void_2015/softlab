package my;

public class Hole extends Obstacle {

	public void execute(Character ch) {
		Main.logger.inc();
		Main.logger.printLog("execute( "+ch+" )");
		Main.logger.nextLevel();
		ch.destroy();
	}

}
