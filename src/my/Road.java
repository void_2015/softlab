package my;

import java.util.ArrayList;

public class Road {

	private ArrayList<Obstacle> obstacles;

	private Rect rect;

    public Road(){

            rect=new Rect();
            obstacles= new ArrayList<Obstacle>();
            obstacles.add(ObstacleFactory.getObstacle());


    }
	
	public void genObstacle() {
        Main.logger.inc();
        Main.logger.printLog("genObstacle()");
        Main.logger.nextLevel();
		ObstacleFactory.getObstacle();

	}

	public void addObstacle(Obstacle o) {
        Main.logger.inc();
        Main.logger.printLog("addObstacle( "+o+" )");
	}

	public Rect getRect() {
		Main.logger.inc();
        Main.logger.printLog("getRect()");

        return rect;
	}

	public Obstacle getObstacle(int index) {
        Main.logger.inc();
        Main.logger.printLog("getObstacle("+index+")");
		return obstacles.get(index);
	}

}
