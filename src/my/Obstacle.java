package my;

public abstract class Obstacle {

	private Rect rect;

    public Obstacle(){
        Main.logger.lock();
        rect= new Rect();
        Main.logger.unlock();
    }
	public abstract void execute(my.Character ch);

	public Rect getRect() {
		Main.logger.inc();
		Main.logger.printLog("getRect()");
		return rect;
	}

}
