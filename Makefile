COMPILER=javac
PACKAGE=src
COMPILER-FLAG=-cp ${PACKAGE}
VERSION=-source 6

build:
	${COMPILER} ${COMPILER-FLAG} $(PACKAGE)/my.my.Main.java
clean:
	rm *.class
