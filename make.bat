@echo off

set PACKAGE_PATH=.\my
set JAVAC=javac -encoding utf8

if -%1-==-- (
	set ARG=#
) else (
	set ARG=%1%
)

if %ARG%==build (
	call :build
) else (
	if %ARG%==clean (
		call :clean	
	) else (
                if %ARG%==run (
                    call :run
                ) else (
                    call :build
                )	
	)
)

exit /b


rem Building
:build

echo Compile the package

cd .\src
for %%i in (%PACKAGE_PATH%\*.java) do (

	
	echo   javac %%i
	%JAVAC% %%i
	
	
)
echo/ 


cd ..

exit /b


rem Cleaning
:clean

cd .\src

echo Clean the package
for %%i in (%PACKAGE_PATH%\*.class) do (

	echo    del %%i
	
	del %%i
	
)
echo/

exit /b

rem Run game
:run
cd ./src
java my.Main
cd ..

exit /b
